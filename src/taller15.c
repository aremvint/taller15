/* $begin sbufc */
#include "include/funciones.h"
int sync_var=0;
int e;
int e_creados=0;
sem_t sem;


void sbuf_init(sbuf_t *sp, int n){
	sp->buf = Calloc(n, sizeof(int)); 
    sp->n = n;                       /* Buffer holds max of n items */
    sp->front = sp->rear = 0;        /* Empty buffer iff front == rear */
    Sem_init(&sp->mutex, 0, 1);      /* Binary semaphore for locking */
    Sem_init(&sp->slots, 0, n);      /* Initially, buf has n empty slots */
    Sem_init(&sp->items, 0, 0);      /* Initially, buf has zero data items */
}
/* $end sbuf_init */

/* Clean up buffer sp */
/* $begin sbuf_deinit */
void sbuf_deinit(sbuf_t *sp){
	Free(sp->buf);
}
/* $end sbuf_deinit */

/* Insert item onto the rear of shared buffer sp */
/* $begin sbuf_insert */
int sbuf_insert(sbuf_t *sp, int item){
	int c;
	if(sync_var){
		P(&sp->slots);                          
		P(&sp->mutex);    
	}                         
	sp->buf[(++sp->rear)%(sp->n)] = item;
	c = (sp->rear-sp->front);
	if(sync_var){
		V(&sp->mutex);                         
		V(&sp->items); 
	}
	return c;  

}
/* $end sbuf_insert */

/* Remove and return the first item from buffer sp */
/* $begin sbuf_remove */
int sbuf_remove(sbuf_t *sp){
	int item;
	if(sync_var){
		P(&sp->items);                          
		P(&sp->mutex);
	}                             
	++sp->front;
	item =(sp->rear-sp->front);
	if(sync_var){
		V(&sp->mutex);                         
		V(&sp->slots);
	}                             
	return item;
}
/* $end sbuf_remove */
/* $end sbufc */

void *thread_productores(void *varg){
	int id=*((int *)varg);
	unsigned int seed = time(NULL);
	while(e_creados<e){
		int n=rand_r(&seed);
		int left = sbuf_insert(&sp, n);
		if(sync_var)
			sem_wait(&sem);
		e_creados++;
		if(sync_var)
			sem_post(&sem);
		printf("Hilo %d produjo un ítem. Hay %d elementos en la cola\n", id, left);
		usleep(1); 
	}
	return NULL;
}

void *thread_consumidores(void *varg){
	int id=*((int *)varg);    
	while(1){
		int c = sbuf_remove(&sp);

		printf("Hilo %d consumió un ítem. Hay %d elementos en la cola\n", id, c);
		usleep(3);
	}
	return NULL;
}
void *thread_promedios(void *varp){
	double inicio= obtenerTiempoActual();
	double final= 0;
	int front = sp.front;
	
	int nRemovidos=0;
	while(1){
		double c=((double)(sp.rear-sp.front))/sp.n;
		printf("El promedio de elementos es: %lf\n", c);
		
		if(sp.front>front){
			
			front = sp.front;
			nRemovidos++;
		}

		if(nRemovidos>0){
			final= obtenerTiempoActual();
			double diferencia = final-inicio;
			
			double promedio = diferencia/(nRemovidos);
			printf("El tiempo promedio es: %lf\n", promedio);
		}else{
			printf("No se ha sacado ningun elemento\n");
		}

	}

}

double obtenerTiempoActual(){
	struct timespec tsp;
	clock_gettime(CLOCK_REALTIME, &tsp);
	double secs=(double)tsp.tv_sec;
	double nano = (double) tsp.tv_nsec/1000000000.0;
	return secs + nano;
}


int main(int argc, char **argv){

	if(argc<9){
		printf("Faltan argumentos. Uso: ./prog -p # -c # -n # -e # [sync]\n");
		abort();
	}
	char **copy=argv;
	int productores=-1;
	int consumidores=-1;
	int tamano_buffer=-1;
	e=-1;
	char c;
	while ((c = getopt (argc, argv, "p:c:n:e:")) != -1){
		if(c=='p'){
			productores = atoi(optarg);
		}else if(c=='c'){
			consumidores = atoi(optarg);
		}else if(c=='n'){
			tamano_buffer = atoi(optarg);
		}else if(c=='e'){
			e = atoi(optarg);
		}else{
			abort();
		}
	}
	
	if(argc>9 && strcmp(copy[9],"sync")==0){
		sync_var=1;
	}
		


	if(productores<=0 || consumidores<=0 || tamano_buffer<=0 ||e <=0){
		abort();
	}
	sem_init(&sem,0,1);
	sbuf_init(&sp,tamano_buffer);
	pthread_t arr[productores];
	for(int i=0; i<productores; i++){
		pthread_t tid;
		int *id=(int *)malloc(sizeof(int));
		*id=i;
		pthread_create(&tid, NULL, thread_productores, id);
		arr[i] = tid;
	}


	for(int i=0; i<consumidores; i++){
		pthread_t tid;
		int *id=(int *)malloc(sizeof(int));
		*id=i;
		pthread_create(&tid, NULL, thread_consumidores, id);
	}    
	pthread_t tid;
	pthread_create(&tid, NULL, thread_promedios, NULL);
	for(int i=0; i<productores; i++){

		pthread_join(arr[i], NULL);
	}



	return 0;
}

