#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <semaphore.h>
#include <pthread.h>
#include <fcntl.h>
#include <string.h>

#define PTHREAD_NUM 2
#define BUFFER_SIZE 100

sem_t sem;

char buffer[BUFFER_SIZE];
int index_consumidor=0;
int index_productor=0;
  

void *consumidor(void *arg)
{
  int valor_semaforo=0;
  while(1)
    {
      sem_wait(&sem);
      //****
      	
      if (buffer[index_consumidor] != '0')
	{
	  printf("Pindex (%d), Cindex (%d), thread[%x]: ", index_productor, index_consumidor,pthread_self());
	  printf("%c\n", buffer[index_consumidor]);
	  
	  buffer[index_consumidor]='0';

	  if(index_consumidor<BUFFER_SIZE)
	    index_consumidor++;
	  else
	    index_consumidor=0;
	}

      //****
      sem_post(&sem);
      sleep(3);
    }
  pthread_exit(0);	 
}

void *productor(void *arg)
{
  int fd;
  char letra[1];
  
  if ((fd = open("text_file", O_RDONLY)) < 0){
    printf("Can't open the file\n");
    exit(0);
  }

  while(1)
    {
      sem_wait(&sem);
      //****

      while ((read(fd, letra, sizeof(letra))) > 0)
	{
	  //printf("Productor trabajando...");
	  if(index_productor < BUFFER_SIZE)
	    {
	      buffer[index_productor] = letra[0];
	      //printf("%c", buffer[index_productor]);

	      index_productor++;
	    }
	  else
	    index_productor=0;

	  sleep(1);
	  //printf("\n");
	  memset(letra,0,sizeof(letra));
	}	
      //****
      sem_post(&sem);

    }
  close(fd);	
  pthread_exit(0);   	
}

int main(void)
{
  pthread_t consumidor_tid[PTHREAD_NUM];
  pthread_t productor_tid;
  int i;

  for (i=0;i<BUFFER_SIZE;i++)
    buffer[i]='0';
  
  if (sem_init(&sem, 0, PTHREAD_NUM) < 0){
    printf("sem_init() failed");
    exit(0);
  }
	
  if (pthread_create(&productor_tid, NULL, productor, NULL) < 0){
    printf("Can't create productor thread\n");
  }
  
  for (i = 0; i < PTHREAD_NUM; i++)
    if (pthread_create(&consumidor_tid[i], NULL, consumidor, NULL) < 0){
      printf("Can't create consumidor thread\n");
      exit(0);
    }
  
  if (pthread_join(productor_tid, NULL) < 0){ 
    printf("Join productor failed\n");
    exit(0);
  }
  
  for (i = 0; i < PTHREAD_NUM; i++)
    if (pthread_join(consumidor_tid[i], NULL) < 0)
      {
  	printf("Join consumidor failed\n");
  	exit(0);
      }

  sem_destroy(&sem);

  return 0;
}


